@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h4 class="alert alert-info"> Edite o Card</h4>
            <form action="" method="post" enctype="multipart/form-data">
                @csrf
                <label for="">Frase do Card: </label><br>
                <input class="form-control" class="form=control"  name="text" value="{{$card->text}}">
                <label for="">Imagem: </label><br>
                <input class="form-control" type="file" name='image' >
                <input type="submit" class="btn btn-warning" value="ATUALIZAR">
            </form>
        </div>
    </div>
</div>

@endsection
