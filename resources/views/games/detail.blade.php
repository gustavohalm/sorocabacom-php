@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-12 text-center">
            <h2>#{{$game->id}} | {{$game->name}} </h2> 
            <br>
            <a href="/games/edit/{{$game->id}}" class="alert alert-warning">EDITAR</a>
            <a href="/games/delete/{{$game->id}}" class="alert alert-danger"> DELETAR</a>
            
            <br><br>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <h6>Imagem de Background: </h6><br>
        <img class="image img" src="{{asset('storage'.$game->background_image)}}" style="max-width:450px;">
        </div>
        
        <div class="col-sm-12 col-md-6">
                <h6>Imagem de Destaque: </h6><br>
            <img class="image img" src="{{asset('storage'. $game->spotlight_image)}}" style="max-width:450px;">
            </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th># </th>
                            <th>Imagem</th>
                            <th>Texto </th>
                            <th>Ação</th>    
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($game->characters as $card)
                            <tr>
                                <td>{{$card->id}}</td>
                                <td><img src="{{asset('storage'.$card->image)}}" alt="" class='image img' style="max-width:450px; "></td>
                                <td> <p>{{$card->text}}</p></td>
                                <td><a href='/character/edit/{{$card->id}}' class='alert alert-success'>EDITAR</a> | 
                                <td><a href='../character/delete/{{$card->id}}' class='alert alert-danger'>DELETAR</a> | 
                                
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td>Não há Cards de Personagem, adicione</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
        </div><br>
        <div class="row">
            <div class="col-sm-12">
                <h4 class="alert alert-info">Adicione os Cards dos personagens</h4>
                <form class="form" action="{{$game->id}}/character/" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="text" name="text" class="form-control" placeholder="Texto do Card">
                    <input type="file" name='image' class="form-control"><br>
                    <input type="submit" value='ADICIONAR' class='btn btn-info'>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
