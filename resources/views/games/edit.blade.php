@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <h4 class="alert alert-info">EDITE O GAME</h4>
            <form action="" method="post" enctype="multipart/form-data">
                @csrf
                <label for="">Nome do Jogo: </label><br>
                <input class="form-control" class="form-control"  name="name" value="{{$game->name}}">
                <label for="">Imagem de Destaque: </label><br>
                <input class="form-control" type="file" name='spotlight_image' >
                <label for="">Imagem de Background: </label><br>
                <input class="form-control" type="file" name='background_image'>
                <label for="">Descrição do Formulário: </label><br>
                <input class="form-control" type="text" name='form_description' value="{{$game->form_description}}">
                <label for="">Frase de destaque: </label><br>
                <input class="form-control" type="text" name='phrase' value="{{$game->phrase}}"><br>
                <input type="submit" class="btn btn-warning" value="ATUALIZAR">
            </form>
        </div>
    </div>
</div>

@endsection
