@extends('layouts.app')

@section('content')


<div class="container">
    <div class="row">
        <div class="text-center col-sm-12">
            <h4 class="alert alert-info"> ADICIONE UM NOVO GAME(Landing Page)</h4>
            <form method="post" action="/games" class="form" enctype="multipart/form-data">
                @csrf
                <label for="">Nome do Jogo: </label><br>
                <input class="form-control" class="name"  name="name">
                <label for="">Imagem de Destaque: </label><br>
                <input class="form-control" type="file" name='spotlight_image' >
                <label for="">Imagem de Background: </label><br>
                <input class="form-control" type="file" name='background_image'>
                <label for="">Descrição do Formulário: </label><br>
                <input class="form-control" type="text" name='form_description'>
                <label for="">Frase de destaque: </label><br>
                <input class="form-control" type="text" name='phrase' >
                <input type="submit" class="btn btn-success" value="ADICIONAR">
            </form>
        </div>
    </div>
        
    <div class="row">
        <div class="col-sm-12"><br>
            <h4>- Meus Games - Landing Pages- </h4>
            <table class='table'>
                <thead>
                    <th>#</th>
                    <th>Name</th>
                    <th>Ação</th>
                </thead>
                <tbody>
                    @forelse($games as $game)
                        <tr>
                            <td><a href='/games/{{$game->id}}'> {{$game->id}} </a> </td>
                            <td>{{$game->name}}</td>
                            <td> <a href="games/{{$game->id}}" class="alert alert-info">DETALHES</a> | <a href="{{url('/games/edit/'.$game->id)}}" class="alert alert-warning">EDITAR</a></td>
                        </tr>
                    @empty
                        <tr>
                            <td>Não há Games cadastrados</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>

        </div>
    </div>
</div>

@endsection
