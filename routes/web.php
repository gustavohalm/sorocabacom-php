<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->group(function() {
    
    Route::get('/', 'GameController@index');
    Route::get('/games/{id}', 'GameController@getOne');
    Route::get('/games/edit/{id}', 'GameController@edit');
    Route::post('/games', 'GameController@store');
    Route::post('/games/edit/{id}', 'GameController@update');
    Route::get('/games/delete/{id}', 'GameController@destroy');
    
    Route::post('/games/{id}/character/', 'CharacterController@store');
    Route::get('/character/edit/{id}', 'CharacterController@edit');
    Route::post('/character/edit/{id}', 'CharacterController@update');
    Route::get('/character/delete/{id}', 'CharacterController@destroy');

});


Auth::routes();


Auth::routes();

