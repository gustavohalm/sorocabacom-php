<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Character;
class Game extends Model
{
    protected $fillable = [
        'name',
        'spotlight_image',
        'background_image',
        'form_description',
        'phrase'
    ];

    public function characters(){
        return $this->hasMany('App\Character', 'game_id');
    }
}
