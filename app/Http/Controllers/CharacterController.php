<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Character;
use App\Traits\UploadTrait;

class CharacterController extends Controller
{
 
    use UploadTrait;

    function store($id, Request $request){
        $card = new Character();
        $request->validate([
            'image'=>'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);
        if($request->has('image')){
            $image = $request->file('image');
            $name = rand(1, 9999)  . rand(10, 888) . rand(100, 599);
            
            $folder = '/uploads/images/';
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            $this->uploadOne($image, $folder, 'public', $name);
            $card->image = $filePath;
        }
        $card->text = $request->input('text');
        $card->game_id = $id;
        $card->save();
        return redirect('/games/'. $id);
    }

    public function edit($id)
    {
        $card = Character::find($id);

        return view('characters.edit', ['card' => $card]);
    }

    public function update(Request $request, $id)
    {
        $card = Character::find($id);
        $request->validate([
            'image'=>'image|mimes:jpeg,png,jpg,gif|max:2048',
            'text' =>'required'
        ]);
        $card->text = $request->input('text');
        $card->save();
        return redirect('/games/'.$card->game_id);
    }
    public function destroy($id)
    {
        Character::destroy($id);

        return redirect('/');
    }
}
