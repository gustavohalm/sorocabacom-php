<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Game;
use App\Character;
use App\Traits\UploadTrait;
class GameController extends Controller
{
    use UploadTrait;
    public function index(){
        $games = Game::all();
        return view('games.home', ['games' => $games]);
    }
    public function getOne($id){
        $game = Game::find($id);
        return view('games.detail', ['game' => $game,]);
    }
    public function create(){
        return view('games.create');
    }
    public function edit($id){
        $game = Game::find($id);
        return view('games.edit', ['game' => $game]);
    }
    public function store(Request $request){
        $game = new Game;
        $request->validate([
            'name'              =>  'required',
            'background_image'     =>  'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'spotlight_image'     =>  'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);
        if ($request->has('background_image')) {
            $image = $request->file('background_image');
            $name = rand(1, 9999)  . rand(10, 888) . rand(100, 599);
            
            $folder = '/uploads/images/';
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            $this->uploadOne($image, $folder, 'public', $name);
            $game->background_image = $filePath;
        }
        if ($request->has('spotlight_image')) {
            $image = $request->file('spotlight_image');
            $name = rand(1, 9999)  . rand(99, 888) . rand(100, 2223);
            $folder = '/uploads/images/';
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            $this->uploadOne($image, $folder, 'public', $name);
            $game->spotlight_image = $filePath;
        }
        $game->name = $request->input('name');
        $game->form_description = $request->input('form_description');
        $game->phrase = $request->input('phrase');
        $game->save();
        return redirect('/');
    }
    public function update(Request $request, $id)
    {
        $game = Game::find($id);
        $request->validate([
            'name'              =>  'required',
            'form_description'  =>  'required',
            'phrase'            =>  'required',
            'background_image'  =>  'image|mimes:jpeg,png,jpg,gif|max:2048',
            'spotlight_image'   =>  'image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);
        if ($request->has('background_image')) {
            $image = $request->file('background_image');
            $name = rand(1, 9999)  . rand(10, 888) . rand(100, 599);
            
            $folder = '/uploads/images/';
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            $this->uploadOne($image, $folder, 'public', $name);
            $game->background_image = $filePath;
        }
        if ($request->has('spotlight_image')) {
            $image = $request->file('spotlight_image');
            $name = rand(1, 9999)  . rand(99, 888) . rand(100, 2223);
            $folder = '/uploads/images/';
            $filePath = $folder . $name. '.' . $image->getClientOriginalExtension();
            $this->uploadOne($image, $folder, 'public', $name);
            $game->spotlight_image = $filePath;
        }
        $game->name = $request->input('name');
        $game->form_description = $request->input('form_description');
        $game->phrase = $request->input('phrase');
        $game->save();
        return redirect('/games/'.$game->id);
    
    }
    public function destroy($id)
    {
        Game::destroy($id);

        return redirect('/');
    }
}
