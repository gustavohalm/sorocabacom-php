Primeiro de tudo é nescesário ter o PHP e mysql instalados.


Clonar o projeto

```
	$ git clone https://gustavohalm@bitbucket.org/gustavohalm/sorocabacom-php.git
	$cd sorocabacom-php
```

 Criando o banco de dados e usuário.
 
```
	$ sudo  mysql
	> CREATE DATABASE db_sorocabacom;
	> CREATE USER 'gustavo'@'localhost' IDENTIFIEED BY 'admin'; // Se usar outro usuario e/ou senha alterar no arquivo .env
	> grant all on db_sorocabacom.*  to 'gustavo'@'localhost';
	>exit
	$ php artisan migrate
```
Rodar o site
```
	php artisan serve
```

